#include <cstdint>
#include <fstream>
#include <iostream>
#include <random>
#include <unistd.h>
#include <fcntl.h>

#include <libdrm/amdgpu.h>
#include <libdrm/amdgpu_drm.h>
#include <xf86drm.h>
#include <cassert>
#include <cstring>
#include <fstream>
#include <array>
#include <set>

struct Buffer {
    std::uint64_t va;
    std::uint64_t size;
    amdgpu_bo_handle handle;
    amdgpu_va_handle va_handle;
    void *ptr;
};

class Amd_device {
public:
    explicit Amd_device(const char *filename);

    ~Amd_device();

    Buffer allocate(std::uint64_t size, bool vram);

    void submit(std::uint64_t va, std::uint64_t size);

private:
    amdgpu_device_handle dev_;
    amdgpu_context_handle ctx_;
};

Amd_device::Amd_device(const char *filename) {
    int fd = open(filename, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "Could not open device file\n");
        throw -1;
    }

    std::uint32_t maj, min;
    int r = amdgpu_device_initialize(fd, &maj, &min, &dev_);
    close(fd);
    if (r) {
        fprintf(stderr, "Couldn't initialize libdrm_amdgpu from device fd\n");
        throw -1;
    }

    r = amdgpu_cs_ctx_create(dev_, &ctx_);
    if (r) {
        amdgpu_device_deinitialize(dev_);
        fprintf(stderr, "Could not create a context\n");
        throw -1;
    }
}

Amd_device::~Amd_device() {
    amdgpu_device_deinitialize(dev_);
}

Buffer Amd_device::allocate(std::uint64_t size, bool vram) {
    Buffer buf = {};
    buf.size = size;

    size = (size + 2 * 1024 * 1024 - 1) & 0xFFFFFFFFFFF00000ULL;
    struct amdgpu_bo_alloc_request alloc_req = {
            .alloc_size = size,
            .phys_alignment = 2 * 1024 * 1024,
            .preferred_heap = (std::uint32_t) (vram ? AMDGPU_GEM_DOMAIN_VRAM : AMDGPU_GEM_DOMAIN_GTT),
            .flags = AMDGPU_GEM_CREATE_VM_ALWAYS_VALID
    };
    int r = amdgpu_bo_alloc(dev_, &alloc_req, &buf.handle);
    if (r) {
        fprintf(stderr, "Failed to allocate buffer\n");
        throw -1;
    }

    r = amdgpu_bo_cpu_map(buf.handle, &buf.ptr);
    if (r) {
        fprintf(stderr, "Failed to map buffer into CPU address space\n");
        amdgpu_bo_free(buf.handle);
        throw -1;
    }

    r = amdgpu_va_range_alloc(dev_, amdgpu_gpu_va_range_general, size, 2 * 1024 * 1024, 0, &buf.va, &buf.va_handle, AMDGPU_VA_RANGE_HIGH);
    if (r) {
        fprintf(stderr, "Failed to allocate VA space for buffer\n");
        amdgpu_bo_free(buf.handle);
        throw -1;
    }

    r = amdgpu_bo_va_op(buf.handle, 0, size, buf.va, 0, AMDGPU_VA_OP_MAP);
    if (r) {
        fprintf(stderr, "Failed to map VA space for buffer\n");
        amdgpu_bo_free(buf.handle);
        amdgpu_va_range_free(buf.va_handle);
        throw -1;
    }
    return buf;
}

void Amd_device::submit(std::uint64_t va, std::uint64_t size) {
    if (size % 32) {
        fprintf(stderr, "cmdbuffer size is not aligned\n");
        throw -1;
    }
    struct amdgpu_cs_ib_info ib = {
            .ib_mc_address = va,
            .size = (uint32_t) (size / 4),
    };
    struct amdgpu_cs_request request = {
            .ip_type = AMDGPU_HW_IP_COMPUTE,
            .ring = 1,
            .number_of_ibs = 1,
            .ibs = &ib,
    };
    int r = amdgpu_cs_submit(ctx_, 0, &request, 1);
    if (r) {
        fprintf(stderr, "submission failed\n");
        throw -1;
    }

    struct amdgpu_cs_fence fence = {
            .context = ctx_,
            .ip_type = AMDGPU_HW_IP_COMPUTE,
            .ring = 1,
            .fence = request.seq_no
    };
    std::uint32_t status, first;
    r = amdgpu_cs_wait_fences(&fence, 1, true, 10000000000ull, &status, &first);
    if (r) {
        fprintf(stderr, "Failed to wait for cmdbuffer completion\n");
        throw -1;
    }
    if (!status) {
        fprintf(stderr, "cmdbuffer didn't compelte in 10 seconds\n");
        throw -1;
    }
}

void pkt3(std::uint32_t *&ptr, unsigned op, unsigned count, unsigned predicate = 0, unsigned shader_type = 0) {
    assert(count >= 1 && count < 0x4002);
    assert(op < 0x100);
    count = (count - 2) & 0x3fff;
    *ptr++ = (0x3u << 30) | (count << 16) | (op << 8) | predicate | (shader_type << 1);
}

void nop(std::uint32_t *&ptr) {
    pkt3(ptr, 0x10, 1);
}

void sh_reg(std::uint32_t *&ptr, unsigned reg, std::uint32_t value) {
    pkt3(ptr, 0x76, 3);
    *ptr++ = (reg - 0xB000) / 4;
    *ptr++ = value;
}

void uconfig_reg(std::uint32_t *&ptr, unsigned reg, std::uint32_t value) {
    pkt3(ptr, 0x79, 3);
    *ptr++ = (reg - 0x30000) / 4;
    *ptr++ = value;
}

const std::uint32_t shader_data[] = {
        0x7e2002ff, 0xdeadbeef,
        0x7e2202ff, 0xdeadbeef,
        0x7e2402ff, 0xdeadbeef,
        0x7e2602ff, 0xdeadbeef,
        0x7e2802ff, 0xdeadbeef,
        0x7e2a02ff, 0xdeadbeef,
        0x7e2c02ff, 0xdeadbeef,
        0x7e2e02ff, 0xdeadbeef,

        0xe038c000, // buffer_load_dwordx4 s[0-3], v[0-3], no index no offset, 0
        0x80400000,
        0xe038c010, // buffer_load_dwordx4 s[0-3], v[4-7], no index no offset, 16
        0x80400400,
        0xe038c020, // buffer_load_dwordx4 s[0-3], v[8-11], no index no offset, 32
        0x80400800,
        0xe038c030, // buffer_load_dwordx4 s[0-3], v[12-15], no index no offset, 48
        0x80400c00,
        0xbf8c0000, // s_waitcnt

#if 1
        0xf19c9f01, // image_bvh_intersect_ray s[8-11], v[0-15], v[16-23]
        0x00021000,
        //0xf1989f01,
       // 0x00021000,
#endif
        0xbf8c0000, // s_waitcnt
        0xe078c000, // buffer_store_dwordx4 s[4-7], v[0-3], no index no offset, 0
        0x80410000,
        0xe078c010, // buffer_store_dwordx4 s[4-7], v[4-7], no index no offset, 16
        0x80410400,
        0xe078c020, // buffer_store_dwordx4 s[4-7], v[8-11], no index no offset, 32
        0x80410800,
        0xe078c030, // buffer_store_dwordx4 s[4-7], v[12-15], no index no offset, 48
        0x80410c00,
        0xe078c040, // buffer_store_dwordx4 s[4-7], v[16-19], no index no offset, 64
        0x80411000,
        0xe078c050, // buffer_store_dwordx4 s[4-7], v[20-23], no index no offset,80
        0x80411400,
        0xbf8c0000, // s_waitcnt
        0xbf810000, // s_endpgm
};


void init_buffer_descriptor(std::uint32_t *data, std::uint64_t va, std::uint32_t stride, std::uint32_t num_records, bool tid) {
    data[0] = va;
    data[1] = ((va >> 32) & 0xffff) | (stride << 16);
    data[2] = num_records;
    data[3] = (1 << 24) | (tid << 23) | (22 /* IMG_FORMAT_32_FLOAT */ << 12) | (4 << 0) | (5 << 2) | (6 << 4) | (7 << 6);
}

void init_bvh_descriptor(std::uint32_t *data, std::uint64_t va, std::uint64_t size, bool sort, bool triangle_ij)
{
   if ((va & 255) || !size || size > (1ull << 42))
      abort();

   data[0] = va >> 8;
   data[1] = ((va >> 40) & 0xff) | ((sort ? 1u : 0u) << 31);
   data[2] = size - 1;
   data[3] = ((size - 1) >> 32) | ((triangle_ij ? 1u : 0u) << 24) | (1u << 31);
}

int main(int argc, char *argv[]) {
    Amd_device dev("/dev/dri/renderD129");
    auto shader = dev.allocate(2 * 1024 * 1024, true);
    std::memcpy(shader.ptr, shader_data, sizeof(shader_data));

    auto bvh = dev.allocate(2 * 1024 * 1024, true);
    memset(bvh.ptr, 0x0, bvh.size);

    auto input = dev.allocate(2 * 1024 * 1024, true);
    memset(input.ptr, 0, input.size);

    std::uint64_t idx = ((((bvh.va) / 64) & ((1ull << 42) - 1)) * 8);
    fprintf(stdout, "va: %llx %llx\n", (long long)bvh.va, (long long)idx);
    auto in_f = (float*)input.ptr;
    ((uint32_t*)input.ptr)[0] = idx;
    ((uint32_t*)input.ptr)[1] = idx >> 32;
    in_f[2] = 10.0;
    in_f[3] = -5.0;
    in_f[4] = -0.98;
    in_f[5] = 0.99;
    in_f[6] = 1;
    in_f[7] = 0;
    in_f[8] = 0;
    in_f[9] = 1;
    in_f[10] = INFINITY;
    in_f[11] = INFINITY;
    auto output = dev.allocate(2 * 1024 * 1024, false);
    memset(output.ptr, 0xe0, output.size);

    auto cmdbuf = dev.allocate(2 * 1024 * 1024, true);
    std::uint32_t *cmd_ptr = (std::uint32_t *) cmdbuf.ptr;
    auto cmd_ptr_base = cmd_ptr;

    sh_reg(cmd_ptr, 0xb810 /* COMPUTE_START_X */, 0);
    sh_reg(cmd_ptr, 0xb814 /* COMPUTE_START_Y */, 0);
    sh_reg(cmd_ptr, 0xb818 /* COMPUTE_START_Z */, 0);

    sh_reg(cmd_ptr, 0xb858 /* COMPUTE_STATIC_THREAD_MGMT_SE0 */, 0xffffffffu);
    sh_reg(cmd_ptr, 0xb85c /* COMPUTE_STATIC_THREAD_MGMT_SE1 */, 0xffffffffu);
    sh_reg(cmd_ptr, 0xb864 /* COMPUTE_STATIC_THREAD_MGMT_SE2 */, 0xffffffffu);
    sh_reg(cmd_ptr, 0xb868 /* COMPUTE_STATIC_THREAD_MGMT_SE3 */, 0xffffffffu);

    uconfig_reg(cmd_ptr, 0x301ec /* CP_COHER_START_DELAY */, 0x20);

    sh_reg(cmd_ptr, 0xb890 /* COMPUTE_USER_ACCUM_0 */, 0);
    sh_reg(cmd_ptr, 0xb894 /* COMPUTE_USER_ACCUM_1 */, 0);
    sh_reg(cmd_ptr, 0xb898 /* COMPUTE_USER_ACCUM_2 */, 0);
    sh_reg(cmd_ptr, 0xb89c /* COMPUTE_USER_ACCUM_3 */, 0);
    sh_reg(cmd_ptr, 0xb8a0 /* COMPUTE_PGM_RSRC3 */, 0);
    sh_reg(cmd_ptr, 0xb9f4 /* COMPUTE_DISPATCH_TUNNEL */, 0);

    sh_reg(cmd_ptr, 0xb854 /* COMPUTE_RESOURCE_LIMITS */, 0x3000001);

    sh_reg(cmd_ptr, 0xb830 /* COMPUTE_PGM_LO */, shader.va >> 8);
    sh_reg(cmd_ptr, 0xb834 /* COMPUTE_PGM_HI */, shader.va >> 40);

    sh_reg(cmd_ptr, 0xb848 /* COMPUTE_PGM_RSRC1 */, (0x1F << 0));
    sh_reg(cmd_ptr, 0xb84C /* COMPUTE_PGM_RSRC2 */, (12 << 1));
    sh_reg(cmd_ptr, 0xb8A0 /* COMPUTE_PGM_RSRC3 */, 0);

    std::uint32_t user_sgpr[12] = {};
    init_buffer_descriptor(user_sgpr + 0, input.va, 128, 128, true);
    init_buffer_descriptor(user_sgpr + 4, output.va, 128, 128, true);
    init_bvh_descriptor(user_sgpr + 8, 0, 1ull << 42, false, true);

    for (unsigned i = 0; i < 12; ++i) {
        sh_reg(cmd_ptr, 0xb900 /* COMPUTE_USER_DATA_0 */ + 4 * i, user_sgpr[i]);
    }

    /* workgroup size */
    sh_reg(cmd_ptr, 0xb81c /* COMPUTE_NUM_THREAD_X */, 1);
    sh_reg(cmd_ptr, 0xb820 /* COMPUTE_NUM_THREAD_Y */, 1);
    sh_reg(cmd_ptr, 0xb824 /* COMPUTE_NUM_THREAD_Z */, 1);

    pkt3(cmd_ptr, 0x15, 3, 0, 1);
    *cmd_ptr++ = 1;
    *cmd_ptr++ = 1;
    *cmd_ptr++ = 1;
    *cmd_ptr++ = 1;

    while (cmd_ptr == cmd_ptr_base || (cmd_ptr - cmd_ptr_base) % 8)
        nop(cmd_ptr);

    std::mt19937 gen(139458);
    std::uniform_int_distribution<std::uint32_t> distrib(0, 0xffffffffu);
    std::uniform_real_distribution<float> distribf(-7.0, 7.0);
    std::set<std::array<std::uint32_t, 4>> results;
    auto bvh_ptr = (uint32_t*)bvh.ptr;
    auto bvh_ptr2 = (float*)bvh.ptr;
    auto out_ptr = (uint32_t*)output.ptr;
    auto out_ptr2 = (float*)output.ptr;


    unsigned m[16] = {0};
    {
      std::ifstream in("/tmp/bvh.bin");
      in.read((char*)bvh.ptr, 13888);
      fprintf(stderr, "root: %d\n", bvh_ptr[0]);
      ///for (unsigned i = 0; i < 6; ++i)
      ///bvh_ptr2[i + 38] = 1.0f;
      for (unsigned i = 16; i < 48; i += 4)
        fprintf(stderr, " %d: %.8x %.8x %.8x %.8x\n", i, bvh_ptr[i], bvh_ptr[i + 1], bvh_ptr[i + 2], bvh_ptr[i + 3]);
    }
    for (unsigned iter = 0; iter < 256; ++iter) {
      float bvh_data[64] = {
#if 1
        -1.000000, -1.000000, -1.000000,
        -1.000000, -1.000000, 1.000000,
        -1.000000, 1.000000, 1.000000,

        0.0, -1.0, -1.0,
        0.0, 0.0, 1.0,
        0.0, 1.0, -1.0,
#endif
        0.0, -1.0, -1.0,
        0.0, -1.0, 3.0,
        0.0, 3.0, -1.0,
      };
     for (unsigned i = 0; i < 12; ++i)
        bvh_ptr2[i] = bvh_data[i];
     bvh_ptr[15] = iter;
      dev.submit(cmdbuf.va, (cmd_ptr - cmd_ptr_base) * sizeof(uint32_t));
      std::array<std::uint32_t, 4> result = {out_ptr[16], out_ptr[17], out_ptr[18], out_ptr[19]};
      //fprintf(stderr, "%x %x %x %x\n", result[0], result[1], result[2], result[3]);

      if (results.find(result) == results.end() && out_ptr2[18] > 3.9 && out_ptr2[19] < 0.2) {
        //fprintf(stdout, "Found change %d:\n", iter);
        fprintf(stdout, "  out %x %lx: %f : %.8x (%f) %.8x (%f) %.8x (%f) %.8x (%f)\n", iter, *((uint64_t*)input.ptr), out_ptr2[16] / out_ptr2[17], out_ptr[16], out_ptr2[16], out_ptr[17], out_ptr2[17], out_ptr[18], out_ptr2[18], out_ptr[19], out_ptr2[19]);
       /* for (unsigned j = 0; j < 32; j += 4)
            fprintf(stdout, "  in%d: %.8x (%f) %.8x (%f) %.8x (%f) %.8x (%f)\n", j, bvh_ptr[j + 0], bvh_ptr2[j+0], bvh_ptr[j + 1], bvh_ptr2[j+1], bvh_ptr[j + 2], bvh_ptr2[j+2],bvh_ptr[j + 3], bvh_ptr2[j+3]);
        fprintf(stdout, "  ray extent: %f\n", in_f[1]);
        fprintf(stderr, "  ray origin: %f %f %f\n", in_f[2], in_f[3], in_f[4]);
        fprintf(stderr, "  ray direction: %f %f %f\n", in_f[5], in_f[6], in_f[7]);
        fprintf(stderr, " ray inv dir: %f %f %f\n", in_f[8], in_f[9], in_f[10]);
        results.insert(result);*/
      }
    }
#if 1
    std::ofstream fout("shader.bin");
    fout.write((const char*)shader_data, sizeof(shader_data));
#endif
    return 0;
}
